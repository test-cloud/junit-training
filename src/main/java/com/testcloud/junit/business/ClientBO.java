package com.testcloud.junit.business;

import java.util.List;

import com.testcloud.junit.business.exception.DifferentCurrenciesException;
import com.testcloud.junit.model.Amount;
import com.testcloud.junit.model.Product;

public interface ClientBO {

	Amount getClientProductsSum(List<Product> products)
			throws DifferentCurrenciesException;

}
